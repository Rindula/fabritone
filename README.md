# Fabritone 

Fork of [Baritone](https://github.com/cabaletta/baritone/) with support for [Fabric](https://github.com/FabricMC).

# Installation

Download the [latest release](https://gitlab.com/deftware/fabritone/-/tags) and put the jar in `.minecraft/mods/`.

Compatible with Minecraft 1.14.4.


**NOTE** the prefix for using baritone with this build is @ not #


